import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'auto-complete-address';

  debugAddressFound(ev: any) {
    console.log('address found', ev);
  }
}
